package com.example.rencio.mobile;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import static com.example.rencio.mobile.R.id.editText2;
import static com.example.rencio.mobile.R.id.textView4;

public class MainActivity extends AppCompatActivity {


    Button mButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mButton = (Button) findViewById(R.id.button);
        mButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //Logic goes here
                Editable text;
                Integer vowels=0;
                String consonant="";
                EditText editText;
                TextView textView1;
                TextView textView2;
                editText=(EditText) findViewById(R.id.editText2);
                text=editText.getText();
                String[] temp = text.toString().split("(?!^)");
                for(int x=0;x<temp.length;x++){
                    if (temp[x].equals("a")||temp[x].equals("A")||temp[x].equals("e")||temp[x].equals("E")||temp[x].equals("i")||temp[x].equals("I")||temp[x].equals("o")||temp[x].equals("O")||temp[x].equals("u")||temp[x].equals("U")){
                        vowels++;}
                    else{
                        consonant=consonant+temp[x];
                    }
                }
                textView1=(TextView) findViewById(R.id.textView4);
                textView1.setText(vowels.toString());
                textView2=(TextView) findViewById(R.id.textView5);
                textView2.setText(consonant.toString());
            }
        });
    }
}
